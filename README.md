# Overview #

While working on both personal and business projects I found myself constantly doing the same steps over and over again whenever I would create a new project. So I decided to jump into unknown world that is "Xcode Project Template documentation" and figure out how to create my own project template.

Most of what I learned was by looking at Apple's own templates (which are located in `/Xcode.app/Contents/Developer/Library/Xcode/Templates/Project Templates/` and `/Xcode.app/Contents/Developer/Platforms/iPhoneOS.platform/Developer/Library/Xcode/Templates/Project Templates/`) but one person in particular really helped me out.

# Installation #

Copy and paste all folders and files to `~/Library/Developer/Xcode/Templates/Custom`. If there no `Custom` folder, just create it and you're done.

![MVCSS Project template](http://i.imgur.com/2JP4b3o.png)

# Structure of project

```
<Project_name>
|
|--AppDelegate.h
|--AppDelegate.m
|--Info.plist
|--Models
     |--
|--Views
     |--
|--Controllers
     |--
|--Stores
     |--
|--Services
     |--
|--Helpers
     |--Util.h
     |--Util.m
     |--Constant.h
     |--Constant.m
|--Resources
     |--Assets.xcassets
     |--Main.storyboard
     |--LaunchScreen.storyboard
|--Supporting Files
     |--main.m
     |--Prefix.pch

```

# So, what's the difference #

- Add prefix file back to import common header files like Constant
- Add class prefix option back in create project form
- Add option to `Allow Arbitrary Loads` (App Transport Security setting)
- Structuring project in MVCSS (Model-View-Controller-Store-Service) design pattern 

# TODO

- Support CocoaPods with default Podfile
- Add option to integrate with Crashlytic by default when create project
- Allow app has multi environment, and switch env without re-build app
- Icon versioning (Show build configuration, git branch, git commit hash and app version in app icon for debugging)

# Credits
MVCSS Project Template is own and maintained by Tran Manh Tuan, member of U1/Posify team, one of fantastic team in MOG Vietnam

# License

MVCSS Project Template is released under the MIT license.