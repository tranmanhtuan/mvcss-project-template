//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//  Copyright (c) ___YEAR___ ___ORGANIZATIONNAME___. All rights reserved.
//

///--------------------------------------
/// @name ENUM DEFINE EXAMPLE
///--------------------------------------

// typedef NS_ENUM(uint8_t, ENUM_NAME) {
//     ENUM_VALUE_0 = 0,
//     ENUM_VALUE_1 = 1,
//     ENUM_VALUE_2 = 2,
//     ENUM_VALUE_3 = 3,
// };

///--------------------------------------
/// @name CONSTANT DEFINE EXAMPLE
///--------------------------------------

/*! @abstract Description for the constant */
// static CGFloat const ___VARIABLE_classPrefix:identifier___floatParameter = 12.0f;

/*! @abstract Description for the constant */
// static NSString * const ___VARIABLE_classPrefix:identifier___stringParameter = @"foo";

/*
 *  System Versioning Preprocessor Macros
 */
#ifndef SYSTEM_VERSION_EQUAL_TO
#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)
#endif